const Social = require('../models/social');
const JsonStorage = require('../jsonStorage');

class SocialRepository {
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
    getSocial() {
        return this.storage.readItems().items;
    }
    getLength() {
        const socials = this.getSocial();
        return parseInt(socials.length);
    }

    getSocialbyId(id) {
        const items = this.storage.readItems().items;
        for (const item of items) {
            if (item.id === id) {
                return item;
            }
        }
        return null;
    }
    addSocial(socialModel) {
        const obj = this.storage.readItems().items;
        const newSocial = new Social(this.storage.readItems().nextId, socialModel.name, socialModel.founder, socialModel.data, parseInt(socialModel.followers), parseInt(socialModel.revenue));
        this.storage.incrementNextId();
        obj.push(newSocial);
        this.storage.writeItems(obj);
        return newSocial;
    }
    updateSocial(socialModel) {
        const obj = this.storage.readItems().items;
        for (const item of obj) {
            if (item.id === socialModel.id) {
                Object.assign(item, socialModel);
            }
        }
        this.storage.writeItems(obj);
        return socialModel;
    }
    deleteSocial(socialid) {
        let count = 0, result;
        const obj = this.storage.readItems().items;
        for (const item of obj) {
            if (item.id === socialid) {
                result = obj[count];
                obj.splice(count, 1);
                break;
            }
            count++;
        }
        this.storage.writeItems(obj);
        return result;
    }
}
module.exports = SocialRepository;