const fs = require('fs');
class JsonStorage {

    constructor(filePath) {
        this.filePath = filePath;
    }

    getnextId() {
        return this.jsonArray.nextId;
    }

    incrementNextId() {
        this.jsonArray.nextId+=1;
    }

    readItems() {
        const jsonText = fs.readFileSync(this.filePath).toString();
        this.jsonArray = JSON.parse(jsonText);
        return this.jsonArray;
    }

    writeItems(newitems) {
        const jsonText = JSON.stringify({ nextId: this.getnextId(), items: newitems }, null, 2);
        fs.writeFileSync(this.filePath, jsonText, 'utf8');
    }
};

module.exports = JsonStorage;