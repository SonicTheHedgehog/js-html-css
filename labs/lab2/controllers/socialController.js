//const Social = require('../models/social');
const SocialRepository = require('../repositories/SocialRepository');
const socialRepository = new SocialRepository("data/social.json");

module.exports = {
    getSocial(req, res) {
        try {
            length = socialRepository.getLength();
            page = 1;
            per_page = 2;
            if (req.query.per_page) {
                if (parseInt(req.query.per_page) > 0 && parseInt(req.query.per_page) <= length)
                    per_page = parseInt(req.query.per_page);
                else res.status(400).send({ message: 'Bad request' });
            }
            if (req.query.page) {
                if (parseInt(req.query.page) > 0 && parseInt(req.query.page) <= (length / per_page)) {
                    page = parseInt(req.query.page);
                }
                else res.status(400).send({ message: 'Bad request' });
            }
            skipped_items = (page - 1) * per_page;
            a = socialRepository.getSocial();
            if (a) {
                a = a.slice(skipped_items, skipped_items + per_page);
                res.status(200).send(a);
            }
            else res.status(404).send({ message: 'not found' });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async getSocialbyId(req, res) {
        try {
            b = socialRepository.getSocialbyId(parseInt(req.params.id));
            if (b) {
                res.status(200).send(b);
            }
            else res.status(404).send({ message: 'not found' });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async addSocial(req, res) {
        try {
            if (!req.body.name || !req.body.date || !req.body.founder) {
                res.status(400).send({ message: 'Bad request' });
            }
            else {
                c = socialRepository.addSocial(req.body);

                if (c) {
                    res.status(201).send({ social_media: c, message: 'ok' });
                }
                else res.status(404).send({ message: "not added" });
            }
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async updateSocial(req, res) {
        try {
            d = socialRepository.updateSocial(req.body);
            if (d) {
                res.status(200).send({ social_media: d, message: 'ok' });
            }
            else res.status(400).send({ message: "not updated" });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async deleteSocial(req, res) {
        try {
            e = socialRepository.deleteSocial(parseInt(req.params.id));
            if (e) {
                res.status(200).send({ social_media: e, message: 'ok' });
            }
            else res.status(400).send({ message: "not deleted" });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    }
};
