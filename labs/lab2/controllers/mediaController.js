const multer = require('multer');
const path = require('path');
const MediaRepository = require('../repositories/mediaRepository');
const mediaRepository = new MediaRepository('data/media');
const upload = multer({
    storage: multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, 'data/media');
        },
        filename: (req, file, cb) => {
            const fileFormat = file.mimetype.split('/')[1];
            cb(null, `${String(mediaRepository.getNextId())}.${fileFormat}`);
        }
    }),
    fileFilter: (req, file, cb) => {
        const tmp = ["jpg", "png", "jpeg", "gif"];
        count = 0;
        for (const item of tmp) {
            if (file.mimetype.split('/')[1] === item) {
                cb(null, true);
                count++;
            }
        }
        if (count != 1) {
            cb(new Error('message: "not appropriate file"'), false);
        }

    }
}).any()

module.exports = {

    async addMedia(req, res) {
        try {
            upload(req, res, (err) => {
                if (err) {
                    console.log('error', err.message);
                    res.status(500).send({ message: 'Error' });
                    return 0;
                } else if (req.files) {
                    res.status(201).send({ mediaId: mediaRepository.getNextId(), message: 'Media has been uploaded' });
                    mediaRepository.incrementId();
                } else {
                    res.status(400).send({ message: 'Bad request' });
                }
            })
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async getMediaById(req, res) {
        try {
            imagepath = mediaRepository.mediaPath(parseInt(req.params.id));
            if (imagepath) {
                res.status(200).sendFile(imagepath, {root: '.'});
            }
            else
                return res.status(404).send({ message: "Not found" });
        }
        catch (err) {
            console.log(err.message);
            res.status(500).send({ error: 'Server error' });
        }
    }

}