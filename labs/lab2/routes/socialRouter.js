const router = require('express').Router();
const socialController = require('../controllers/socialController');

router
    /**
     * @route GET /api/socials/{id}
     * @group Social - network operations
     * @param {integer} id.path.required - id of the Social - eg: 1
     * @returns {Social.model} 200 - Social network object
     * @returns {Error} 404 - Social network not found
     */
    .get("/:id", socialController.getSocialbyId)
    /**
 * @route GET /api/socials
 * @group Social - network operations
 * @returns {Social.model} 200 - Social network object
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<Social>} Socials networks - a page with networks
 * @returns {Error} 404 - Social network not found
 */
    .get("/", socialController.getSocial)
    /**
 * @route POST /api/socials
 * @group Social - social network operations
 * @param {Social.model} id.body.required - new Social network object
 * @returns {Social.model} 201 - added Social network object
 * @returns {Error} 400 - bad request
 * @returns {Error} 404 - Social network not added
 */
    .post("/", socialController.addSocial)
    /**
 * @route PUT /api/socials
 * @group Social - Social network operations
 * @param {Social.model} id.body.required - new Social network object
 * @returns {Social.model} 200 - changed Social network object
 * @returns {Error} 404 - Social network not updated
 */
    .put("/", socialController.updateSocial)
    /**
* @route DELETE /api/socials/{id}
* @group Social - Social network operations
* @param {integer} id.path.required - id of the Social network - eg: 1
* @returns {Social.model} 200 - deleted Social network object
* @returns {Error} 404 - Social network not deleted
*/
    .delete("/:id", socialController.deleteSocial)

module.exports = router;