const express = require('express')
const app = express()

const apiRouter = require('./routes/apiRouter');
app.use('/api', apiRouter);

const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);
 
const options = {
    swaggerDefinition: {
        info: {
            description: 'Some description',
            title: 'Some swagger',
            version: '1.0.0',
        },
        host: 'localhost:3000',
        produces: [ "application/json" ],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(options);

app.listen(3000, function () {
    console.log('Server is ready');
});

