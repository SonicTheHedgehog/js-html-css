const User = require('../models/user');
 
class UserRepository {
 
    async getUsers() { 
        const users = await User.find();
        return users;
    }
    async getLength()
    {
        const users = await User.find();
        return parseInt(users.length);
    }
    async getUserById(id) {
        const user = await User.findById(id);
        return user;
    }
};
 
module.exports = UserRepository;

