const Social = require('../models/social');
const cons = require('consolidate');
const { insertMany } = require('../models/social');

class SocialRepository {

    async getSocial() {
         const socials = await Social.find();
        return socials;
    }
    async getLength() {
        const socials = await Social.find();
        return parseInt(socials.length);
    }
    async getSocialbyId(id) {
        const socials = await Social.findById(id);
        return socials;
    }
    async getLengthbyName(name) {
        let pages_total = 0;
        const socials = await this.getSocialbyName(name);
        const pages = Number.parseInt(socials.length);
        if(pages==1){
            pages_total = 1;
        }
        else {
            if ((pages / 2) - Math.trunc(pages / 2) != 0) {
                pages_total = Math.trunc(pages / 2) + 1;
            }
            else {
                pages_total = Math.trunc(pages / 2);
        }
    }
        return pages_total;
    }
    async getSocialbyName(namee)
    {
        const social = await Social.find();
        const socials = social.map(soc=>soc.toJSON());
        let result=[];
        const length =await this.getLength();
        for(let i=0; i< length; i++)
        {
            if (socials[i].name.includes(namee))
            {
                result.push(socials[i]);
            }
        }
        return result;
    }
    async addSocial(socialModel) {
        const social = new Social(socialModel);
        await Social.insertMany(social);
        return social;
    }
    async getLastSocials() {

        const soc = await Social.find().sort({'date': -1}).limit(4);
        return soc.map(netw => netw.toJSON());

    }
    updateSocial(socialModel) {
        const obj = this.storage.readItems().items;
        for (const item of obj) {
            if (item.id === socialModel.id) {
                Object.assign(item, socialModel);
            }
        }
        this.storage.writeItems(obj);
        return socialModel;
    }
    async deleteSocial(socialid) {
        const social = await Social.deleteOne({_id:socialid});
    }
}
module.exports = SocialRepository;