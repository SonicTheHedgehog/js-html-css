const router = require('express').Router({mergeParams:true});
const socialController = require('../controllers/socialController');
const groupRouter = require('./groupRouter');

router
   
    .get("/new", (req, res) => { res.status(200).render('new') })
    .get("/:id", socialController.getSocialbyId)
    .get("/", socialController.getSocial)
    .post("/", socialController.addSocial);
    // .post("/:id", socialController.deleteSocial)
    // .get("/:id/groups/newg", (req, res) => { res.status(200).render('newg',{social_id:req.params.id}) })
    // .use('/:id/groups', groupRouter);
    
   

module.exports = router;