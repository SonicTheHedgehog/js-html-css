const greatRouter = require('express').Router();
const socialRouter = require('./socialRouter');
const apiRouter = require('./apiRouter');
const groupRouter = require('./groupRouter');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');

greatRouter
    .use(bodyParser.urlencoded({ extended: true }))
    .use(bodyParser.json())
    .use(busboyBodyParser())
    .use("/api/socials", apiRouter)
    .use('/socials', socialRouter)
    .use('/groups',groupRouter);

module.exports = greatRouter;