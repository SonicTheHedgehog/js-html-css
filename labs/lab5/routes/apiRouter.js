const router = require('express').Router({mergeParams:true});
const socialController = require('../controllers/socialController');

router
    .get("/new", (req, res) => { res.status(200).render('new',{user_id:req.params.id}) })
    .get("/:id", socialController.getApiSocialbyId)
    .get("/", socialController.getApiSocial)
    .post("/", socialController.addApiSocial)
    .post("/:id", socialController.deleteApiSocial);   

module.exports = router;