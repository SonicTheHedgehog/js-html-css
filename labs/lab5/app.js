const path = require('path');
const express = require('express');
const app = express();
const mongoose= require('mongoose');
require("dotenv").config();
const http = require("http");
const ws = require("ws");
const dbUrl = process.env.DBLINK;
const connectOptions = {
   useNewUrlParser: true,
   useUnifiedTopology: true,
};


const morgan = require('morgan');
app.use(morgan('dev'));

const mustache = require('mustache-express');
const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');
    
app.use(express.static('public'));
app.use(express.static('data'));
app.get('/', function (req, res) {res.render('index');});
app.get('/about', function (req, res) {res.render('about');});


let webSocketConnections = require("./utils/socketsColl").socketCollection;

const server = http.createServer(app);
const wsServer = new ws.Server({ server });

wsServer.on("connection", (connection) => {
    console.log(webSocketConnections);
    webSocketConnections.push(connection);
    console.log("(+) new connection. total connections:", webSocketConnections.length);

    connection.on("close", () => {
        webSocketConnections.splice(webSocketConnections.indexOf(connection), 1);
        console.log(
            "(-) connection lost. total connections:",
            webSocketConnections.length
        );
    });
});

const greatRouter = require('./routes/greatRouter');
app.use('', greatRouter);
app.use((req, res) => {res.status(400).send({message: 'Incorrect routing'});});

server.listen(process.env.PORT || 3000, async () => {
    try {
         console.log('Server ready');
         const client = await mongoose.connect(dbUrl, connectOptions);
         console.log('Mongo database connected');
    } catch (error) {
         console.log(error);
    }
 });