const fs = require('fs');
const path = require('path');
const SocialRepository = require('../repositories/socialRepository');
const socialRepository = new SocialRepository();
const Group = require('../models/group');
let webSocketConnections = require("../utils/socketsColl").socketCollection;
require("dotenv").config();
const cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: process.env.CLOUDINARY,
    api_key: process.env.KEY,
    api_secret: process.env.SECRET
});
async function uploadRaw(buffer) {
    return new Promise((resolve, reject) => {
        cloudinary.v2.uploader
            .upload_stream({ resource_type: 'raw' },
                (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(result);
                    }
                })
            .end(buffer);
    });
}

module.exports = {
        async  getSocial(req, res) {
        try {
            res.status(200).render("socials");
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async getSocialbyId(req, res) {
        try {
            let b = await socialRepository.getSocialbyId(req.params.id);
            if (b) {
                res.status(200).render("social", { social: b });;
            }
            else res.status(404).send({ message: 'not foundd' });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },
    async addSocial(req, res) {
        try {
            console.log(7);
            const result = await uploadRaw(req.files['photo'].data);
            console.log(result.url);
            req.body.photo = result.url;
            //req.body.user_id=req.params.id;
            const newSoc = await socialRepository.addSocial(req.body);
            const newSocId = newSoc._id;
            res.redirect(`/socials`);
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },


    /////////////////////////////////////////////////////////////////////////////////


    async  getApiSocial(req, res) {
        try {
            length = await socialRepository.getLength();
            page = 1;
            per_page = 2;
            pages_total = 0;
            lastSoc = await socialRepository.getLastSocials();
            social = await socialRepository.getSocial();
            let name = "";
            
            if (req.query.social && req.query.social != "") {
                name = req.query.social;
            }
            if (name != "") {
                social = await socialRepository.getSocialbyName(name);
                pages_total = await socialRepository.getLengthbyName(name);
            }
            else
            {
                if ((length / per_page) - Math.trunc(length / per_page) != 0) {
                    pages_total = Math.trunc(length / per_page) + 1;
                }
                else pages_total = Math.trunc(length / per_page);
            }
            if (pages_total === 0) {
                pages_total = 1;
            }

            if (req.query.per_page) {
                if (parseInt(req.query.per_page) > 0 && parseInt(req.query.per_page) <= length)
                    per_page = parseInt(req.query.per_page);
                else res.status(400).send({ message: 'Bad request' });
            }
            if (req.query.page) {
                if (parseInt(req.query.page) > 0 && parseInt(req.query.page) <= pages_total) {
                    page = parseInt(req.query.page);
                }
                else res.status(400).send({ message: 'Bad request' });
            }
            skipped_items = (page - 1) * per_page;
            prevPage = 0;
            nextPage = 0;
            if (page != 1)
                prevPage = page - 1;
            if (page != pages_total)
                nextPage = page + 1;
            page_info = { pages_total, currentPage: page, prevPage: prevPage, nextPage: nextPage, pageSearch: name };
            if (social) {
                social = social.slice(skipped_items, skipped_items + per_page);
                const socData = { socials: social, page_info, lastSoc };
                res.status(200).send(JSON.stringify(socData));
            }
            else res.status(404).send({ message: 'not fouund' });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    }, 

    async getApiSocialbyId(req, res) {
        try {
            let b = await socialRepository.getSocialbyId(req.params.id);
            if (b) {
                res.status(200).render("social", { social: b });;
            }
            else res.status(404).send({ message: 'not foundd' });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async addApiSocial(req, res) {
        try {
            console.log(7);
            const result = await uploadRaw(req.files['photo'].data);
            console.log(result.url);
            req.body.photo = result.url;
            const newSoc = await socialRepository.addSocial(req.body);
            for (const connection of webSocketConnections) {
                connection.send(JSON.stringify(newSoc));
            }
            res.send(JSON.stringify(newSoc));
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async deleteApiSocial(req, res) {
        try {
            let e =await socialRepository.deleteSocial(req.params.id);
            res.redirect('/socials');
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    }
};
