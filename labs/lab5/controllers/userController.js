const UserRepository = require('../repositories/userRepository');
const userRepository = new UserRepository();

module.exports = {
    async getUsers(req, res) {
        try {
            length =await userRepository.getLength();
            page = 1;
            per_page = 2;
            if (req.query.per_page) {
                if (parseInt(req.query.per_page) > 0 && parseInt(req.query.per_page) <= length)
                    per_page = parseInt(req.query.per_page);
                else res.status(400).send({ message: 'Bad request' });
            }
            if (req.query.page) {
                if (parseInt(req.query.page) > 0 && parseInt(req.query.page) <= (length / per_page)) {
                    page = parseInt(req.query.page);
                }
                else res.status(400).send({ message: 'Bad request' });
            }
            skipped_items = (page - 1) * per_page;
            a = await userRepository.getUsers();
            if (a) {
                a = a.slice(skipped_items, skipped_items + per_page);
                res.status(200).render("users",{users:a});
            }
            else res.status(404).send({ message: 'not found' });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async getUserById(req, res) {
        try {
            b = await userRepository.getUserById(req.params.id);
            if (b) {
                res.status(200).render("user",{user:b});
            }
            else res.status(404).send({ message: 'not found' });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    }
};
