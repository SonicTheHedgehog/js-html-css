let tableStr = "";
let paginationStr = "";
let loadingStr = "";
document.addEventListener('DOMContentLoaded', (event) => {
    console.log('DOM fully loaded and parsed');
    fetch("/templates/table.mst")
        .then((x) => x.text())
        .then((y) => tableStr = y)
        .catch((err) => console.error(err));
    fetch("/templates/pagination.mst")
        .then((x) => x.text())
        .then((y) => paginationStr = y)
        .catch((err) => console.error(err));
    fetch("/templates/loading.mst")
        .then((x) => x.text())
        .then((y) => {loadingStr = y; console.log(loadingStr)})
        .catch((err) => console.error(err));
});
function uploadData(currentPage, pageName) {
    const mainel = document.getElementById("themain");
    mainel.innerHTML = loadingStr;
    fetch(`/api/socials?page=${currentPage}&social=${pageName}`).then((x) => x.json())
        .then((jsonObject) => {
            const itemsData = jsonObject.socials;
            const renderedPaginationStr = Mustache.render(paginationStr, {
                page_info: jsonObject.page_info,
                pages_total: jsonObject.pages_total
            });
            const renderedHtmlStr = Mustache.render(tableStr, {
                socials: itemsData,
                pagination: renderedPaginationStr
            });
            return renderedHtmlStr;
        })
        .then((htmlStr) => {
            const mainel = document.getElementById("themain");
            mainel.innerHTML = htmlStr;
            const prev = document.getElementById("prev");
            const next = document.getElementById("next");
            const totpage = document.getElementById("currpage");
            const exactpage = document.getElementById("exactpage");
            const searchname = document.getElementById("searchname")
            if (currentPage == 1) {
                prev.classList.add("disabledBut");
            }
            if (currentPage == Number.parseInt(totpage.textContent)) {
                next.classList.add("disabledBut");
            }
            exactpage.oninput = () => {
                uploadData(exactpage.value, pageName);
            }
            searchname.oninput = () => {
                uploadData(1, searchname.value);
            }      

            prev.addEventListener('click', () => {
                uploadData(currentPage - 1, pageName);
            })
            next.addEventListener('click', () => {
                uploadData(currentPage + 1, pageName);
            })
        })
        .catch((err) => console.error(err));
}

uploadData(1, "");