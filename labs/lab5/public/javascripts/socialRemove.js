let removedStr = "";
let loadingStr = "";
document.addEventListener('DOMContentLoaded', (event) => {
    console.log('DOM fully loaded and parsed');
    fetch("/templates/socialRemove.mst")
        .then((x) => x.text())
        .then((y) => removedStr = y)
        .catch((err) => console.error(err));
    fetch("/templates/loading.mst")
        .then((x) => x.text())
        .then((y) => {loadingStr = y; console.log(loadingStr)})
        .catch((err) => console.error(err));
});
function deleteData() {
    const modalWindow = document.getElementById("exampleModal");
    bootstrap.Modal.getInstance(modalWindow).hide();
    const mainDiv = document.getElementById("socmain");
    mainDiv.innerHTML = loadingStr;
    fetch(`/api/socials/${window.location.href.split('/')[4]}`, {
        method: "POST"
     })
    .then(() => {
        const htmlStr = Mustache.render(removedStr);
        const mainDiv = document.getElementById("socmain");
        mainDiv.innerHTML = htmlStr;
    })
    .catch((err) => console.error(err));
}

const deleteButton = document.getElementById("truedelete");
deleteButton.onclick = () => {
    deleteData();
}