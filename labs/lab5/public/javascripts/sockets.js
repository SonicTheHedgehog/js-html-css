const protocol = location.protocol === "https:" ? "wss:" : "ws:";
const wsLocation = `${protocol}//${location.host}`;
const connection = new WebSocket(wsLocation);
let socketStr = "";
document.addEventListener('DOMContentLoaded', (event) => {
    console.log('DOM fully loaded and parsed');
    fetch("/templates/sockets.mst")
        .then((x) => x.text())
        .then((y) => socketStr = y)
        .catch((err) => console.error(err));
});

function showToastMessage(messageText) {
    console.log(messageText);
    const newItem = JSON.parse(messageText.data);
            console.log(newItem);
            const appEl = document.getElementById("push-toasts");
            const date = new Date();
            const time = date.getHours() + ":" + date.getMinutes();
            console.log(socketStr);
            appEl.insertAdjacentHTML('beforeend', Mustache.render(socketStr, { id: newItem._id, name: newItem.name, date: time, photo: newItem.photo}));
            console.log(appEl);
            const toastEl = document.getElementById("liveToast" + newItem._id);
            const newItemHref = document.getElementById("newItem" + newItem._id);
            const btnX = document.getElementById("buttonX" + newItem._id);
            btnX.addEventListener('click', () => {
                document.getElementById("liveToast" + newItem._id).classList.add("hide"); 
            });
            newItemHref.href = `/socials/${newItem._id}`;
            const toastOption = {
                animation: true,
                autohide: true,
                delay: 1000000,
            };
            const toast1 = new bootstrap.Toast(toastEl, toastOption);
            toast1.show();
}
connection.addEventListener("open", () =>
    console.log("Connected to ws server:" , wsLocation));
connection.addEventListener("error", () => console.error("ws error"));
connection.addEventListener("message", (message) => {showToastMessage(message);});