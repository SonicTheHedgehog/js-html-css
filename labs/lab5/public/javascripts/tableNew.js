let tableNewStr = "";
let loadingStr = "";
document.addEventListener('DOMContentLoaded', (event) => {
    console.log('DOM fully loaded and parsed');
    fetch("/templates/tableNew.mst")
        .then((x) => x.text())
        .then((y) => tableNewStr = y)
        .catch((err) => console.error(err));
    fetch("/templates/loading.mst")
        .then((x) => x.text())
        .then((y) => loadingStr = y)
        .catch((err) => console.error(err));
});
function uploadData() {
    const oldsoc = document.getElementById("oldsoc");
    oldsoc.innerHTML = loadingStr;
        fetch("/api/socials").then((x) => x.json())
        .then(( allSocials) => {
            const renderedHtmlStrList = Mustache.render(tableNewStr, {
                socials: allSocials.lastSoc,
            });
            return renderedHtmlStrList;
        })
        .then((htmlStr) => {
            const oldsoc = document.getElementById("oldsoc");
            oldsoc.innerHTML = htmlStr;
            const inputSubmit = document.getElementById("addNew");
            const currentForm = document.getElementById("formNew");
            inputSubmit.onclick = () => {
                currentForm.onsubmit = function (el) {
                    el.preventDefault();
                    const formData = new FormData(this);
                    postData(formData);
                    currentForm.reset();
                }
            };
        })
        .catch((err) => console.error(err));
}

function postData(formData) {
    const inputSubmit = document.getElementById("addNew");
    const currentForm = document.getElementById("formNew");
    inputSubmit.removeAttribute("onclick");
    currentForm.removeAttribute("onsubmit");
    const oldsoc = document.getElementById("oldsoc");
    oldsoc.innerHTML = loadingStr;
    fetch("/api/socials", {method: "POST",body: formData,})
    .then((x) => x.json())
    .then(() => {
        uploadData();
    })
    .catch((err) => console.error(err));
}

uploadData();