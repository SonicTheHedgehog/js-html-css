const readline = require('readline-sync');
const UserRepository = require('./repositories/userRepository');
const userRepository = new UserRepository("data/user.json");
const SocialRepository = require('./repositories/SocialRepository');
const socialRepository = new SocialRepository("data/social.json");
while (true) {
    const input = readline.question("Enter command:");
    const parts = input.split("/");
    const begg = parts[0];
    const command = parts[1];
    if (begg === "get") {
        if (command === "users" && !parts[2]) {
            const allusers = userRepository.getUsers();
            for (const item of allusers) {
                console.log(item.id + ". login: " + item.login + ", fullname: " + item.fullname);
            }
        }
        else if (command === "user" && !isNaN(parseInt(parts[2]))) {
            const userId = parseInt(parts[2]);
            const user = userRepository.getUserById(userId);
            if (!user) {
                console.log(`Error: user with id ${userId} not found.`);
            }
            else
                console.log("fullname: " + user.fullname, ",\nrole: " + user.role, ", \nregistration: " + user.registeredAt, ", \nava: " + user.avaUrl);
        }
        else if (command === "socials" && !parts[2]) {
            const allsocials = socialRepository.getSocial();
            for (const item of allsocials) {
                console.log(item.id + ". name: " + item.name + ", \nfounder: " + item.founder + ", \nfollowers: " + item.followers);
            }
        }
        else if (command === "socials" && !isNaN(parseInt(parts[2]))) {
            const socId = parseInt(parts[2]);
            const soc = socialRepository.getSocialbyId(socId);
            if (!soc) {
                console.log(`Error: user with id ${socId} not found.`);
            }
            else console.log("name: " + soc.name + ", \nfounder: " + soc.founder + ", \nfollowers: " + soc.followers);
        }
        else {
            console.log(`Not supported command`);

        }
    }
    else if (command === "socials") {
        if (begg === "delete" && !isNaN(parseInt(parts[2]))) {
            const socId = parseInt(parts[2]);
            const soc = socialRepository.getSocialbyId(socId);
            if (!soc) {
                console.log(`Error: user with id ${socId} not found.`);
            }
            else {
                socialRepository.deleteSocial(socId);
                console.log("Done");
            }
        }
        else if (begg === "update" && !isNaN(parseInt(parts[2]))) {
            const socId = parseInt(parts[2]);
            const soc = socialRepository.getSocialbyId(socId);
            if (!soc) {
                console.log(`Error: user with id ${socId} not found.`);
            }
            else {
                const input = readline.question("Write throught a comma:\nname,founder,date,followers,revenues:\n");
                const parse = input.split(',');
                if (!parse[5] && !isNaN(parseInt(parse[3])) && !isNaN(parseInt(parse[4])) && !isNaN(Date.parse(parse[2]))) {
                    const obj = { id: socId, name: parse[0], founder: parse[1], date: parse[2], followers: parseInt(parse[3]), revenues: parseInt(parse[4]) };
                    socialRepository.updateSocial(obj);
                }
                else { console.log(`Incorrect input`); }
            }
        }
        else if (begg === "post" && !parts[2]) {
            const input = readline.question("Write throught a comma:\nname,founder,date,followers,revenues:\n");
            const parse = input.split(',');
            if (!parse[5] && !isNaN(parseInt(parse[3])) && !isNaN(parseInt(parse[4])) && !isNaN(Date.parse(parse[2]))) {
                const obj = {id:null, name: parse[0], founder: parse[1], date: parse[2], followers: parseInt(parse[3]), revenues: parseInt(parse[4]) };
                socialRepository.addSocial(obj);
            }
            else { console.log(`Incorrect input`); }
        }
        else {
            console.log(`Not supported command`);
        }
    }
    else {
        console.log(`Not supported command`);
        break;
    }

}