const User = require('../models/user');
const JsonStorage = require('../jsonStorage');
 
class UserRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
 
    getUsers() { 
        return this.storage.readItems().items;
    }
 
    getUserById(id) {
        const items = this.storage.readItems().items;
        for (const item of items) {
            if (item.id === id) {
            return item;
            }
        }
        return null;
    }
};
 
module.exports = UserRepository;

