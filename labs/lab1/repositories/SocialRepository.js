const Social = require('../models/social');
const JsonStorage = require('../jsonStorage');

class SocialRepository {
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
    getSocial() {
        return this.storage.readItems().items;
    }
    getSocialbyId(id) {
        const items = this.storage.readItems().items;
        for (const item of items) {
            if (item.id === id) {
                return item;
            }
        }
        return null;
    }
    addSocial(socialModel) {
        const obj = this.storage.readItems().items;
        const nid = this.storage.readItems().nextId;
        socialModel.id = nid;
        obj.push(socialModel);
        this.storage.incrementNextId();
        this.storage.writeItems(obj);
    }
    updateSocial(socialModel) {
        const obj = this.storage.readItems().items;
        for (const item of obj) {
            if (item.id === socialModel.id) {
                Object.assign(item, socialModel);
            }
        }
        this.storage.writeItems(obj);
    }
    deleteSocial(socialid)
    {
        let count=0;
        const obj = this.storage.readItems().items;
        for (const item of obj) {
            if (item.id === socialid) {
                obj.splice(count,1);
            }
            count++;
        }
        this.storage.writeItems(obj);
    }
}
module.exports = SocialRepository;