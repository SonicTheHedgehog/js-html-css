const router = require('express').Router();

const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
const userRouter = require('./userRouter');
const socialRouter = require('./socialRouter');
const groupRouter = require('./groupRouter');

router
    .use(bodyParser.urlencoded({ extended: true }))
    .use(bodyParser.json())
    .use(busboyBodyParser())

    .use('/users', userRouter)
    .use('/socials', socialRouter)
    .use('/groups', groupRouter)
    

module.exports = router;
