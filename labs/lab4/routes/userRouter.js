const router = require('express').Router();
const userController = require('../controllers/userController');
const socialRouter = require('./socialRouter');

router
    
    .get("/:id", userController.getUserById)
    .get("/", userController.getUsers)
    .use('/:id/socials', socialRouter);

module.exports = router;
