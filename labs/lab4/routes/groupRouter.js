const router = require('express').Router({mergeParams:true});
const groupController = require('../controllers/groupController')

router
   

    .get("/:id", groupController.getGroupbyId)
    .get("/", groupController.getGroup)
    .post("/", groupController.addGroup)
    .post("/:id", groupController.deleteGroup)
    

module.exports = router;