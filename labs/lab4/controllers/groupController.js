const fs = require('fs');
const path = require('path');
const GroupRepository = require('../repositories/groupRepository');
const groupRepository = new GroupRepository();
const Group = require('../models/group');
const extra = require('fs-extra');
require("dotenv").config();
const cloudinary = require('cloudinary');
const SocialRepository = require('../repositories/socialRepository');
const socialRepository = new SocialRepository();
cloudinary.config({
    cloud_name: process.env.CLOUDINARY,
    api_key: process.env.KEY,
    api_secret: process.env.SECRET
});

module.exports = {
    async getGroup(req, res) {
        try {
            length = await groupRepository.getLength(req.params.id);
            page = 1;
            per_page = 2;
            pages_total = 0;
            group = await groupRepository.getGroup(req.params.id);
            let name="";
            if (req.query.name && req.query.name != "") {
                name = req.query.name;
            }
            if (name != "") {
                group = await groupRepository.getGroupbyName(name);
            }

            if ((length / per_page) - Math.trunc(length / per_page) != 0) {
                pages_total = Math.trunc(length / per_page) + 1;
            }
            else pages_total = Math.trunc(length / per_page);
            if (pages_total === 0) {
                pages_total = 1;
            }

            if (req.query.per_page) {
                if (parseInt(req.query.per_page) > 0 && parseInt(req.query.per_page) <= length)
                    per_page = parseInt(req.query.per_page);
                else res.status(400).send({ message: 'Bad request' });
            }
            if (req.query.page) {
                if (parseInt(req.query.page) > 0 && parseInt(req.query.page) <= pages_total) {
                    page = parseInt(req.query.page);
                }
                else res.status(400).send({ message: 'Bad request' });
            }
            skipped_items = (page - 1) * per_page;
            prevPage = 0;
            nextPage = 0;
            if (page != 1)
                prevPage = page - 1;
            if (page != pages_total)
                nextPage = page + 1;
            page_info = { pages_total, currentPage: page, prevPage: prevPage, nextPage: nextPage, pageSearch: name };
            if (group) {
                group = group.slice(skipped_items, skipped_items + per_page);
                res.status(200).render("groups", { groups: group, page_info, user_id: req.params.id, social_id: req.params.id });
            }
            else res.status(404).send({ message: 'not fouund' });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async getGroupbyId(req, res) {
        try {
            b = await groupRepository.getGroupbyId(req.params.id);
            c = await socialRepository.getSocialbyId(b.social_id);
            d = await c.name;
            if (b) {
                res.status(200).render("group", { group: b, socname: d });
            }
            else res.status(404).send({ message: 'not foundd' });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async addGroup(req, res) {
        try {
            req.body.social_id = req.params.id;
            const newGrId = await groupRepository.addGroup(req.body);
            res.redirect(`/socials/${req.body.social_id}/groups/${newGrId}`);
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },


    async deleteGroup(req, res) {
        try {
            e = await groupRepository.deleteGroup(req.params.id);
            res.redirect('./');
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    }
};
