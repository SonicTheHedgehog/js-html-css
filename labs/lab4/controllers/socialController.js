const fs = require('fs');
const path = require('path');
const SocialRepository = require('../repositories/socialRepository');
const socialRepository = new SocialRepository();
const Group = require('../models/group');
const extra = require('fs-extra');
require("dotenv").config();
const cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: process.env.CLOUDINARY,
    api_key: process.env.KEY,
    api_secret: process.env.SECRET
});
async function uploadRaw(buffer) {
    return new Promise((resolve, reject) => {
        cloudinary.v2.uploader
            .upload_stream({ resource_type: 'raw' },
                (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(result);
                    }
                })
            .end(buffer);
    });
}

module.exports = {
        async  getSocial(req, res) {
        try {
            length = await socialRepository.getLength(req.params.id);
            page = 1;
            per_page = 2;
            pages_total = 0;
            social = await socialRepository.getSocial(req.params.id);
            console.log(req.params.id);
            let name = "";
            if (req.query.name && req.query.name != "") {
                name = req.query.name;
            }
            if (name != "") {
                social = await socialRepository.getSocialbyName(name, req.params.id);
            }

            if ((length / per_page) - Math.trunc(length / per_page) != 0) {
                pages_total = Math.trunc(length / per_page) + 1;
            }
            else pages_total = Math.trunc(length / per_page);
            if (pages_total === 0) {
                pages_total = 1;
            }

            if (req.query.per_page) {
                if (parseInt(req.query.per_page) > 0 && parseInt(req.query.per_page) <= length)
                    per_page = parseInt(req.query.per_page);
                else res.status(400).send({ message: 'Bad request' });
            }
            if (req.query.page) {
                if (parseInt(req.query.page) > 0 && parseInt(req.query.page) <= pages_total) {
                    page = parseInt(req.query.page);
                }
                else res.status(400).send({ message: 'Bad request' });
            }
            skipped_items = (page - 1) * per_page;
            prevPage = 0;
            nextPage = 0;
            if (page != 1)
                prevPage = page - 1;
            if (page != pages_total)
                nextPage = page + 1;
            page_info = { pages_total, currentPage: page, prevPage: prevPage, nextPage: nextPage, pageSearch: name };
            if (social) {
                social = social.slice(skipped_items, skipped_items + per_page);
                res.status(200).render("socials", { socials: social, page_info, user_id:req.params.id });
            }
            else res.status(404).send({ message: 'not fouund' });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async getSocialbyId(req, res) {
        try {
            b = await socialRepository.getSocialbyId(req.params.id);
            if (b) {
                res.status(200).render("social", { social: b });;
            }
            else res.status(404).send({ message: 'not foundd' });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async addSocial(req, res) {
        try {
            console.log(7);
            const result = await uploadRaw(req.files['photo'].data);
            console.log(result.url);
            req.body.photo = result.url;
            req.body.user_id=req.params.id;
            console.log(9);
            const newSoc = await socialRepository.addSocial(req.body);
            const newSocId = newSoc._id;
            console.log(newSocId);
            res.redirect(`/users/${req.body.user_id}/socials/${newSocId}`);
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async updateSocial(req, res) {
        try {
            d = await socialRepository.updateSocial(req.body);
            if (d) {
                res.status(200).render("social", { social_media: d, message: 'ok' });
            }
            else res.status(400).send({ message: "not updated" });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async deleteSocial(req, res) {
        try {
            e =await socialRepository.deleteSocial(req.params.id);
            await Group.deleteMany({social_id: req.params.id});
            res.redirect('./');
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    }
};
