const mongoose = require('mongoose');
const GroupSchema = new mongoose.Schema({
    name: { type: String, required: true },
    users: { type: Number },
    admin: { type: String },
    social_id:{type: mongoose.Types.ObjectId , ref: 'Social', required: true }
});

const GroupModel = mongoose.model('Group', GroupSchema);
 
module.exports = GroupModel;