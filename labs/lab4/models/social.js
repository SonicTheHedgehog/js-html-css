const mongoose = require('mongoose');
const SocialSchema = new mongoose.Schema({
    name: { type: String, required: true },
    founder: { type: String, required: true },
    date: { type: String, default: Date.now, required: true },
    followers: { type: Number },
    revenue: { type: Number },
    photo: { type: String },
    user_id: { type: mongoose.Types.ObjectId , ref: 'User', required: true }
});

const SocialModel = mongoose.model('Social', SocialSchema);
 
module.exports = SocialModel;