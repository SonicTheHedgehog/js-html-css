const Group = require('../models/group');
const cons = require('consolidate');
const { insertMany } = require('../models/group');

class GroupRepository {

    async getGroup() {
        const groups = await Group.find();
        return groups.map(group=>group.toJSON());
    }
    async getLength() {
        const groups = await Group.find();
        return parseInt(groups.length);
    }
    async getGroupbyId(id) {
        const groups = await Group.findById(id);
        return groups.toJSON();
    }
    async addGroup(groupModel) {
        const groups = new Group(groupModel);
        await Group.insertMany(groups);
        return groups._id;
    }
    async getGroupbyName(namee) {
        const groups = await this.getGroup();
        let result = [];
        for (let i = 0; i < await this.getLength(); i++) {
            if (groups[i].name.includes(namee)) {
                result.push(groups[i]);
            }
        }
        return result;
    }
    async deleteGroup(groupid) {
        const social = await Group.deleteOne({ _id: groupid });
    }
}
module.exports = GroupRepository;