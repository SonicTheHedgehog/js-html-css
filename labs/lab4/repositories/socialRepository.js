const Social = require('../models/social');
const cons = require('consolidate');
const { insertMany } = require('../models/social');

class SocialRepository {

    async getSocial(id) {
         const socials = await Social.find({user_id:id});
        return socials;
    }
    async getLength(id) {
        const socials = await Social.find({user_id:id});
        return parseInt(socials.length);
    }
    async getSocialbyId(id) {
        const socials = await Social.findById(id);
        return socials;
    }
    async getSocialbyName(namee, id)
    {
        const social = await Social.find({user_id:id});
       
        const socials = social.map(soc=>soc.toJSON());

        let result=[];
        for(let i=0; i< await this.getLength(id); i++)
        {
            if (socials[i].name.includes(namee))
            {
                result.push(socials[i]);
            }
        }
        return result;
    }
    async addSocial(socialModel) {
        const social = new Social(socialModel);
        await Social.insertMany(social);
        return social._id;
    }
    updateSocial(socialModel) {
        const obj = this.storage.readItems().items;
        for (const item of obj) {
            if (item.id === socialModel.id) {
                Object.assign(item, socialModel);
            }
        }
        this.storage.writeItems(obj);
        return socialModel;
    }
    async deleteSocial(socialid) {
        const social = await Social.deleteOne({_id:socialid});
    }
}
module.exports = SocialRepository;