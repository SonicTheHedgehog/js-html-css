//const Social = require('../models/social');
const fs = require('fs');
const path = require('path');
const SocialRepository = require('../repositories/SocialRepository');
const socialRepository = new SocialRepository("data/social.json");
const MediaRepositore = require('../repositories/mediaRepository');
const Social = require('../models/social');
const mediaRepository = new MediaRepositore("data/media");

module.exports = {
    getSocial(req, res) {
        try {
            length = socialRepository.getLength();
            page = 1;
            per_page = 2;
            pages_total = 0;
            social = socialRepository.getSocial();
            name = "";
            if (req.query.name && req.query.name != "") {
                name = req.query.name;
            }
            if (name != "") {
                social = socialRepository.getSocialbyName(name);
            }

            if ((length / per_page) - Math.trunc(length / per_page) != 0) {
                pages_total = Math.trunc(length / per_page) + 1;
            }
            else pages_total = Math.trunc(length / per_page);
            if (pages_total === 0) {
                pages_total = 1;
            }

            if (req.query.per_page) {
                if (parseInt(req.query.per_page) > 0 && parseInt(req.query.per_page) <= length)
                    per_page = parseInt(req.query.per_page);
                else res.status(400).send({ message: 'Bad request' });
            }
            if (req.query.page) {
                if (parseInt(req.query.page) > 0 && parseInt(req.query.page) <= pages_total) {
                    page = parseInt(req.query.page);
                }
                else res.status(400).send({ message: 'Bad request' });
            }
            skipped_items = (page - 1) * per_page;
            prevPage = 0;
            nextPage = 0;
            if (page != 1)
                prevPage = page - 1;
            if (page != pages_total)
                nextPage = page + 1;
            page_info = { pages_total, currentPage: page, prevPage: prevPage, nextPage: nextPage, pageSearch: name };
            if (social) {
                social = social.slice(skipped_items, skipped_items + per_page);
                res.status(200).render("socials", { socials: social, page_info });
            }
            else res.status(404).send({ message: 'not fouund' });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async getSocialbyId(req, res) {
        try {
            b = socialRepository.getSocialbyId(parseInt(req.params.id));
            if (b) {
                res.status(200).render("social", { social: b });;
            }
            else res.status(404).send({ message: 'not foundd' });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async addSocial(req, res) {
        try {
            const fileFormat = req.files['photo'].mimetype.split('/')[1];
            fs.writeFileSync(path.resolve(__dirname, '../data/media/' + mediaRepository.getNextId() + '.' + fileFormat), req.files['photo'].data, (err) => {
                if (err) { console.log("Can't load this photo."); }
            })
            req.body.photo = '/media/' + mediaRepository.getNextId() + '.' + fileFormat;
            const news = socialRepository.addSocial(req.body);
            mediaRepository.incrementId();
            if (news) {
                res.redirect('socials/' + news.id);
            }
            else res.status(404).send({ message: "not added" });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async updateSocial(req, res) {
        try {
            d = socialRepository.updateSocial(req.body);
            if (d) {
                res.status(200).render("social", { social_media: d, message: 'ok' });
            }
            else res.status(400).send({ message: "not updated" });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    },

    async deleteSocial(req, res) {
        try {
            e = socialRepository.deleteSocial(parseInt(req.params.id));
            if (e) {
                res.redirect("/socials");
            }
            else res.status(400).send({ message: "not deleted" });
        }
        catch {
            res.status(500).send({ error: 'Server error' });
        }
    }
};
