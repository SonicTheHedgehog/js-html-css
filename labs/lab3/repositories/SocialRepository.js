const Social = require('../models/social');
const JsonStorage = require('../jsonStorage');
const cons = require('consolidate');

class SocialRepository {
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
    getSocial() {
        return this.storage.readItems().items;
    }
    getLength() {
        const socials = this.getSocial();
        return parseInt(socials.length);
    }
    getSocialbyId(id) {
        const items = this.storage.readItems().items;
        for (const item of items) {
            if (item.id === id) {
                return item;
            }
        }
        return null;
    }
    getSocialbyName(namee)
    {
        const socials = this.getSocial();
        let result=[];
        for(let i=0; i<this.getLength(); i++)
        {
            if (socials[i].name.includes(namee))
            {
                result.push(socials[i]);
            }
        }
        return result;
    }
    addSocial(socialModel) {
        const obj = this.storage.readItems().items;
        const newSocial = new Social(this.storage.readItems().nextId, socialModel.name, socialModel.founder, socialModel.date, parseInt(socialModel.followers), parseInt(socialModel.revenue), socialModel.photo);
        this.storage.incrementNextId();
        obj.push(newSocial);
        this.storage.writeItems(obj);
        return newSocial;
    }
    updateSocial(socialModel) {
        const obj = this.storage.readItems().items;
        for (const item of obj) {
            if (item.id === socialModel.id) {
                Object.assign(item, socialModel);
            }
        }
        this.storage.writeItems(obj);
        return socialModel;
    }
    deleteSocial(socialid) {
        let count = 0, result;
        const obj = this.storage.readItems().items;
        for (const item of obj) {
            if (item.id === socialid) {
                result = obj[count];
                obj.splice(count, 1);
                break;
            }
            count++;
        }
        this.storage.writeItems(obj);
        return result;
    }
}
module.exports = SocialRepository;