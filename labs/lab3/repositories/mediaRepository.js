const JsonStorage = require('../jsonStorage');
const fs = require('fs');

class MediaRepository {
    constructor(filePath) {
        this.path = filePath;
        this.storage = new JsonStorage(filePath + ".json");
    }
    getNextId() {
        return this.storage.readItems().nextId;
    }
    incrementId() {
        const tmp = this.storage.readItems().items;
        this.storage.incrementNextId();
        this.storage.writeItems(tmp);
    }
    mediaPath(id) {
        for (const format of this.storage.readItems().items.fileFormats) {
            const fullPath =   this.path + '/' + String(id) + '.' + format;
            if (fs.existsSync(fullPath)) {
                return fullPath;
            }
        }
        return undefined;
    }
}

module.exports = MediaRepository;