const router = require('express').Router();

const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
const userRouter = require('./userRouter');
const socialRouter = require('./socialRouter');
const mediaRouter = require('./mediaRouter');

router
    .use(bodyParser.urlencoded({ extended: true }))
    .use(bodyParser.json())
    .use(busboyBodyParser())

    .use('/users', userRouter)
    .use('/socials', socialRouter)
    .use('/media', mediaRouter)

module.exports = router;
