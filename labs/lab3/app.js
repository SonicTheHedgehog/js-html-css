const path = require('path');
const express = require('express');
const app = express();

const morgan = require('morgan');
app.use(morgan('dev'));

const mustache = require('mustache-express');
const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');
    
app.use(express.static('public'));
app.use(express.static('data'));
app.get('/', function (req, res) {res.render('index');});
app.get('/about', function (req, res) {res.render('about');});

const apiRouter = require('./routes/apiRouter');
app.use('', apiRouter);
app.use((req, res) => {res.status(400).send({message: 'Incorrect routing'});});

app.listen(3000, function () {
    console.log('Server is ready');
});




/*const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);
 
const options = {
    swaggerDefinition: {
        info: {
            description: 'Some description',
            title: 'Some swagger',
            version: '1.0.0',
        },
        host: 'localhost:3000',
        produces: [ "application/json" ],
    },
    basedir: __dirname,*/
//    files: ['./routes/**/*.js', './models/**/*.js'],};
//expressSwagger(options);