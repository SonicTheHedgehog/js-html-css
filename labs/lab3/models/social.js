/**
 * @typedef Social
 * @property {integer} id
 * @property {string} name.required - social network name
 * @property {string} founder.required - person, who has founded 
 * @property {string} date.required - date of foundation
 * @property {integer} followers - number of the followers
 * @property {integer} revenue - revenue of the social network
 */
class Social
{
    constructor(id, name, founder,date,followers,revenue,photo)
    {
    this.id=id;
    this.name=name;
    this.founder=founder;
    this.date=date;
    this.followers=followers;
    this.revenue=revenue;
    this.photo = photo;
    }
};

module.exports = Social;